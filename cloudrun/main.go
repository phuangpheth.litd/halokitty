package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/shopspring/decimal"

	_ "github.com/lib/pq"
)

func getEnv(key, fallback string) string {
	if value := os.Getenv(key); value != "" {
		return value
	}
	return fallback
}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Println(msg, err)
		os.Exit(1)
	}
}

func main() {
	ctx := context.Background()
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbURL := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbName)

	db, err := sql.Open("postgres", dbURL)
	failOnError(err, "failed to open database")
	defer func() {
		err := db.Close()
		failOnError(err, "failed to close database")
	}()

	h := &handler{db: db}
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, echo.Map{
			"message": "Hello, this is a message from Golang running on CloudRun.",
		})
	})
	e.GET("/products", h.ListProducts)

	errCh := make(chan error, 1)
	go func() {
		errCh <- e.Start(fmt.Sprintf(":%s", getEnv("PORT", "3001")))
	}()

	ctx, cancel := signal.NotifyContext(ctx, os.Kill, os.Interrupt)
	defer cancel()

	select {
	case err := <-errCh:
		if err != nil && err != http.ErrServerClosed {
			log.Fatal("failed to start server")
		}
		log.Println("server shutdown gracefully")

	case <-ctx.Done():
		ctx, cancel := context.WithTimeout(ctx, time.Second*15)
		defer cancel()

		log.Println("shutting down server...")
		if err := e.Shutdown(ctx); err != nil {
			log.Fatal("failed to shutdown server")
		}
		log.Println("shutdown server gracefully")
	}
}

type handler struct {
	db *sql.DB
}

type Product struct {
	ID        string          `json:"id"`
	Name      string          `json:"displayName"`
	Currency  string          `json:"currency"`
	Price     decimal.Decimal `json:"price"`
	Quantity  decimal.Decimal `json:"quantity"`
	CreatedAt time.Time       `json:"createdAt"`
	UpdatedAt time.Time       `json:"updatedAt"`
}

func (h *handler) ListProducts(c echo.Context) error {
	products, err := listProducts(c.Request().Context(), h.db)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{
		"products": products,
	})
}

func listProducts(ctx context.Context, db *sql.DB) ([]Product, error) {
	query := `SELECT id, display_name, currency, price, qty, created_at, updated_at FROM products`

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("db.QueryContext(): %w", err)
	}

	products := make([]Product, 0)
	for rows.Next() {
		var p Product
		if err := rows.Scan(&p.ID, &p.Name, &p.Currency, &p.Price, &p.Quantity, &p.CreatedAt, &p.UpdatedAt); err != nil {
			return nil, fmt.Errorf("rows.Scan(): %w", err)
		}
		products = append(products, p)
	}
	return products, nil
}
